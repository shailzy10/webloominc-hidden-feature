import React, { useState } from 'react';
import { CircularProgress } from '@mui/material/';

import './NotLoggedIn.css';

interface Props {
  loginEventListener: any;
}

const NotLoggedIn: React.FC<Props> = ({ loginEventListener }: Props) => {
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  function getUserToken() {
    setIsLoggingIn(true);
    chrome.runtime.sendMessage({ message: 'signIn' }, function (sendResponse) {
      if (sendResponse.message === 'Successful') {
        localStorage.setItem('id_token', sendResponse.data.accessToken);
        localStorage.setItem('refresh_token', sendResponse.data.refreshToken);
        localStorage.setItem('access_token', sendResponse.auth_token);
      }

      setIsLoggingIn(false);
      loginEventListener();
    });
  }

  return (
    <div className="container">
      <p className="container__title">Swiggy pH</p>
      <h1 className="container__meeting">Meetings made better</h1>
      {isLoggingIn ? (
        <div className="container_loader">
          <CircularProgress className="loader" />
        </div>
      ) : (
        <button className="container__btn" onClick={() => getUserToken()}>
          Connect Google Calendar
        </button>
      )}
    </div>
  );
};

export default NotLoggedIn;
