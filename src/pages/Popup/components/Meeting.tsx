import React from 'react';
import SendIcon from '@mui/icons-material/Send';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import moment from 'moment';

const Meeting = ({ meeting }: any) => {
  function sendMeetingDetails(details: any) {
    chrome.tabs.query(
      { active: true, currentWindow: true },
      function (tabs: any[]) {
        chrome.tabs.sendMessage(tabs[0].id, {
          message: JSON.stringify(meeting),
        });
      }
    );
  }

  return (
    <div
      className="meeting__container"
      onClick={() => sendMeetingDetails(meeting)}
    >
      <div className="meeting__schedule">
        <p className="meeting_schedule_duration">
          {moment(meeting.startTime).format('LT')} -{' '}
          {moment(meeting.endTime).format('LT')}
        </p>
        {meeting && meeting.meeting_link && meeting.meeting_link !== '' ? (
          <h3
            className="meeting_join_link"
            onClick={() => window.open(meeting.meeting_link, '_blank')}
          >
            Join Meeting
          </h3>
        ) : null}
      </div>
      <h3 className="meeting__headline">{meeting.name}</h3>

      <div className="meeting__bottom">
        <div className="meetingIcons">
          <div className="meetingBtn meeting__am">
            <p className="meetingBtn_text">AM</p>
          </div>

          <div className="meetingBtn meeting__es">
            <p className="meetingBtn_text">ES</p>
          </div>
        </div>

        <div className="attachment__container">
          <AttachFileIcon className="attachBtn" />
          <p className="attachment__text"> 10 attachments </p>
        </div>
      </div>
    </div>
  );
};

export default Meeting;
