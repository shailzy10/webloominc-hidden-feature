import React, { useState, useEffect } from 'react';
import './LoggedIn.css';
import Meeting from './Meeting';
import { CircularProgress } from '@mui/material/';
import { Refresh, LogoutOutlined } from '@mui/icons-material/';

interface Props {
  loginEventListener: any;
}

const LoggedIn: React.FC<Props> = ({ loginEventListener }: Props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [date, setDate] = useState(0);
  const [day, setDay] = useState('');
  const [suffix, setSuffix] = useState('');
  const [meetings, setMeetings] = useState<any[]>([]);

  function logOutUser() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('access_token');
    loginEventListener();
  }

  function refreshMeetingData() {
    setIsLoading(true);
    getDay();
    fetchMeetings();
  }

  function fetchMeetings() {
    chrome.runtime.sendMessage(
      { message: 'fetchMeetings' },
      function (sendResponse) {
        if (sendResponse.message === 'Successful') {
          console.log(sendResponse.data.results);
          setMeetings(sendResponse.data.results);
          setIsLoading(false);
        }
      }
    );
  }
  const getDay = () => {
    const d = new Date().getDate();
    const days = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const day = new Date().getDay();
    setDate(d);
    setDay(days[day]);
    if (d === 2 || d === 12 || d === 22) {
      setSuffix('nd');
    } else if (d === 3 || d === 13 || d === 23) {
      setSuffix('rd');
    } else {
      setSuffix('th');
    }
  };
  useEffect(() => {
    refreshMeetingData();
  }, []);
  return (
    <div className="container">
      <div className="container__header">
        <h2>
          {date}
          <small>{suffix}</small> {day}
        </h2>
        <div className="settings">
          <Refresh className="icon" onClick={() => refreshMeetingData()} />
          <LogoutOutlined className="icon" onClick={() => logOutUser()} />
        </div>
        {/* <h3 className="settings">Settings</h3> */}
      </div>

      <div className="blue--border" />
      {/* <div style={{ width: 400 }}> */}
      <button
        className="createMeetingBtn"
        onClick={() => window.open('https://calendar.google.com/', '_blank')}
      >
        + Create New Meeting
      </button>
      <div className="meetings__overallContainer">
        {isLoading ? (
          <CircularProgress />
        ) : meetings.length >= 1 ? (
          meetings.map((meeting) => (
            <Meeting key={meeting.id} meeting={meeting} />
          ))
        ) : (
          <p>You have no upcoming meetings</p>
        )}
      </div>
      {/* </div> */}
    </div>
  );
};

export default LoggedIn;
