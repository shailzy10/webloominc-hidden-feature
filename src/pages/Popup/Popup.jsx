import React, { useEffect, useState } from 'react'
import NotLoggedIn from './components/NotLoggedIn'
import LoggedIn from "./components/LoggedIn"

const Popup = () => {
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);

  useEffect(() => {
    loginEventListener();
  }, []);

  const loginEventListener = () => {
    const isUserAvailable = localStorage.getItem("access_token") && localStorage.getItem("refresh_token") ? true : false;
    setIsUserLoggedIn(isUserAvailable);
  }

  return (
    <div>
        { isUserLoggedIn ? 
          <LoggedIn loginEventListener={loginEventListener} /> : <NotLoggedIn loginEventListener={loginEventListener} />
        }
    </div>
  )
}

export default Popup
