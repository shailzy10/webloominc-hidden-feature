import React from 'react';

const AgendaComponent = ({ summary, calendarLink }) => {
  return (
    <div className="agenda">
      <p className="agenda__header">Meeting Description</p>
      <div className="agenda__underline" />

      {summary && summary !== '' ? (
        <div className="agenda__details">
          <p className="agenda__header">Agenda -</p>
          <div
            dangerouslySetInnerHTML={{ __html: summary.substring(0, 240) }}
            className="agenda__todo"
          ></div>

          <a
            href={calendarLink}
            target={'_blank'}
            className="agenda__header more__link"
          >
            ...read more
          </a>
        </div>
      ) : (
        <div className="agenda_details_empty">
          <p className="agenda_details_empty_text">
            No context available for the meeting
          </p>
        </div>
      )}
    </div>
  );
};

export default AgendaComponent;
