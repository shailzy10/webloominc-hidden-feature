import React from 'react';
import Agenda from '../SvgComponents/Agenda.js';
import LightAgenda from '../SvgComponents/LightAgenda.js';
import Alarm from '../SvgComponents/Alarm.js';
import LightAlarm from '../SvgComponents/LightAlarm.js';
import Hide from '../SvgComponents/Hide.js';
import LightHide from '../SvgComponents/LightHide.js';
import Notes from '../SvgComponents/Notes.js';

const PopupHeader = ({
  toggleAlarm,
  toggleAgenda,
  isPlaying,
  hideContents
}) => {
  return (
    <div className="btnContainer">
      <div className="btn" onClick={toggleAlarm}>
        {isPlaying ? <LightAlarm /> : <Alarm />}
        <p className={isPlaying ? 'btn__text--alarm' : 'btn__text'}>
          {isPlaying ? 'Stop ' : 'Start '}Alarm
        </p>
      </div>

      <div className="btn" onClick={toggleAgenda}>
        {isPlaying ? <LightAgenda /> : <Agenda />}
        <p className={isPlaying ? 'btn__text--alarm' : 'btn__text'}>Agenda</p>
      </div>

      <div className="btn">
        <Notes />
        <p className={isPlaying ? 'btn__text--alarm' : 'btn__text'}>Notes</p>
      </div>

      <div className="btn" onClick={hideContents}>
        {isPlaying ? <LightHide /> : <Hide />}
        <p className={isPlaying ? 'btn__text--alarm' : 'btn__text'}>Hide</p>
      </div>
    </div>
  );
};

export default PopupHeader;
