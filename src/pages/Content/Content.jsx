import { LinearProgress } from '@mui/material';
import moment from 'moment';
import React, {useState, useRef, useEffect} from 'react';
import AgendaComponent from './components/AgendaComponent';
import PopupHeader from './components/PopupHeader';
import Hide from './SvgComponents/Hide';
import LightHide from './SvgComponents/LightHide';
import './content.styles.css';

const Content = () => {
  const [timer, setTimer] = useState(60);
  const [showAgenda, setShowAgenda] = useState(false);
  const countRef = useRef(null);
  const toggleAgenda = () => setShowAgenda(!showAgenda);
  const [isPlaying, setIsPlaying] = useState(false);
  const [meetings, setMeetings] = useState(null);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [currentTime, setCurrentTime] = useState(null);
  const [started, setStarted] = useState(false);
  const [show, setShow] = useState(false);
  const [meetingDuration, setMeetingDuration] = useState(null);
  const [duration, setDuration] = useState(null);
  const [hidden, setHidden] = useState(false);

  function hideContents () {
    setHidden(true);
  }
  function unhideContents () {
    setHidden(false);
  }

  useEffect(() => {
    if(window && window.location && window.location.pathname) {
      chrome.storage.sync.get(['bearer_token'], function(result) {
        fetch(`https://ce.finow.io/v1/calendar/meeting/?meeting=${window.location.origin}${window.location.pathname}`, {
          method : "GET",
          headers: {
            Authorization: `Bearer ${result.bearer_token}`
          }
        }).then(response => response.json())
        .then(data => {
          setMeetings(data);
        }).catch(err => {
          console.log({
            message : "Failed to get meetings",
            error: err
          })
        })
      });
    }
  }, [window.location.pathname]);
 
  useEffect(()=> {
    if(meetings && meetings.id) {
      const meetingObject = meetings;
      
      setShow(true);
      setMeetings(meetingObject);
      setStartTime(meetingObject.startTime);
      setEndTime(meetingObject.endTime);
      const time = new Date();
      const hasMeetingStarted = moment().isAfter(moment(meetingObject.startTime));
      const startHour = moment(meetingObject.startTime).format('X');
      const endHour = moment(meetingObject.endTime).format('X');
      let diffTime = (endHour - startHour);
      let duration = moment.duration(diffTime * 1000, 'milliseconds');
      setMeetingDuration(duration);
      setStarted(hasMeetingStarted);
      setCurrentTime(time);
    }
  }, [meetings]);

  function triggerAlarm() {
    toggleAlarm();
    setTimeout(() => {
      toggleAlarm("close");
    }, 5000)
  }

  function timeRemainingToStart() {
    const startHour = moment(startTime).format('X');
    const endHour = moment(endTime).format('X');
    const currentHour = moment().format('X');

    const hasMeetingStarted = moment().isAfter(moment(startTime));
    let diffTime = started ? (endHour - currentHour) : (startHour - currentHour);
    let duration = moment.duration(diffTime * 1000, 'milliseconds');

    if (started) {
      if (duration && duration._data && meetingDuration) {
        if(((duration._data.days * 24 + duration._data.hours) * 60 + duration._data.minutes) === Math.floor(((meetingDuration._data.days * 24 + meetingDuration._data.hours) * 60 + meetingDuration._data.minutes) / 2)) {
          if (((meetingDuration._data.days * 24 + meetingDuration._data.hours) * 60 + meetingDuration._data.minutes) % 2 === 0) {
            triggerAlarm();
          } else if(duration._data.seconds === 30) {
            triggerAlarm();
          }
        }
  
        if (duration && duration._data.days === 0 && duration._data.hours === 0 && duration._data.seconds === 0 && duration._data.minutes === 5) {
          triggerAlarm();
        }
  
        if (duration && duration._data.days === 0 && duration._data.hours === 0 && duration._data.seconds === 0 && duration._data.minutes === 0) {
          triggerAlarm();
        }
      }
    }
    
    setDuration(duration);
    setStarted(hasMeetingStarted);
  }

  function toggleAlarm(message) {
    let alarmTone = new Audio("http://www.ofhsmath.com/vmath/APCS/student_projects/Charboneau/Sean%27s%20Mario%20(Oiram)/oneUp.mp3");

    if(!isPlaying && message !== "close") {
      alarmTone.volume = 0.7;
      alarmTone.muted = false;
      setIsPlaying(true);
      alarmTone.play();
      
    } else {
      setIsPlaying(false);
      alarmTone.pause();
      alarmTone.currentTime = 0;
    }
    
  }

  const stopTimer = () => {
    clearInterval(countRef.current);
    setTimer(0);
}
  useEffect(()=> {
    if(currentTime) {
      const countDown = () => {
        countRef.current = setInterval(()=> {
          timeRemainingToStart();
        }, 1000);
      }
      countDown();
    }
  }, [currentTime])

  useEffect(()=>{
  }, [duration])
  
  return (
    <>
    <div className="app" id = "app">
      {show ? 
      (
        <>
        <div className={`${hidden ? "hideButton_shown" : "hideButton__hidden"} ${isPlaying ? "hideButton--alarm" : "hideButton"}`}  onClick={unhideContents}>
        {isPlaying ? <LightHide /> : <Hide />}
        <p className={isPlaying ? 'btn__text--alarm' : 'btn__text'}>Unhide</p>
        </div>
        <div className={hidden ? "popup__hidden" : "popup__shown"}>
        <div className={isPlaying ? "popup--alarm" : "popup"} id = "popup">
            <div className="header__container">
              <div className="header__title">
                <h3 className={isPlaying ? "popup__title--alarm" : "popup__title"}>Swiggy pH</h3>
              </div>
              <PopupHeader toggleAlarm={toggleAlarm} toggleAgenda={toggleAgenda} isPlaying={isPlaying} hideContents={hideContents}/>                 
            </div>
      
            <div className="timer">
                  <h1 className={isPlaying ? "timer__reading--alarm" : "timer__reading"}>
                  {duration && duration._data ? `${Math.abs(duration._data.days * 24 + duration._data.hours) <= 9 ? `0${Math.abs(duration._data.days * 24 + duration._data.hours)}` : Math.abs(duration._data.days * 24 + duration._data.hours)}:${Math.abs(duration._data.minutes) <= 9 ? `0${Math.abs(duration._data.minutes)}` : Math.abs(duration._data.minutes)}` : "00:00"}
                  </h1>
                  <p className={isPlaying ? "timer__left--alarm" : "timer__left"}>
                  {(duration && duration._data && (duration._data.days * 24 + duration._data.hours) === 0 && duration._data.minutes === 0 ) && started ? "Time out" : (duration && duration._data && (duration._data.hours < 0 || duration._data.minutes < 0 )) ? "Over Time" : started ? "mins left" : "Yet to start"}
                  </p>
                  {
                    started && duration && meetingDuration ?
                    // started && (duration && duration._data && (duration._data.days * 24 + duration._data.hours) > 0 && duration._data.minutes > 0 ) ?
                    <LinearProgress variant={'determinate'} value={100 * (duration._milliseconds / meetingDuration._milliseconds)} className='popup_live_timer' classes={{
                      bar: 'popup_live_timer_bar'
                    }} /> : null
                  }
                  
              </div>
            <div className="bar"/>
            
        </div>
           {showAgenda ? <AgendaComponent summary={meetings.summary} calendarLink={meetings.calendar_link} /> : " " }
           </div>
        </>

      ) : (
        null
      )
      }  
      </div>
      </>
  );
};

export default Content;
