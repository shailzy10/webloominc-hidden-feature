const firebase = require('firebase/app');
const firebaseAuth = require('firebase/auth');
try {
  const firebaseConfig = {
    apiKey: 'AIzaSyCowpDMs3EokXabXiEDx0udSZXC6iN3AQ0',
    authDomain: 'swiggy-ph.firebaseapp.com',
    projectId: 'swiggy-ph',
    storageBucket: 'swiggy-ph.appspot.com',
    messagingSenderId: '122395525301',
    appId: '1:122395525301:web:530a46c5705e4756dffefc',
    measurementId: 'G-XVSEZJBQRQ',
  };

  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const auth = firebaseAuth.getAuth(firebaseApp);
  const GoogleAuthProvider = firebaseAuth.GoogleAuthProvider;
  const provider = new GoogleAuthProvider();

  chrome.runtime.onMessage.addListener(function (
    request,
    sender,
    sendResponse
  ) {
    if (request.message === 'signIn') {
      chrome.identity.getAuthToken(
        { interactive: true },
        function (auth_token) {
          let credential = GoogleAuthProvider.credential(null, auth_token);
          firebaseAuth
            .signInWithCredential(auth, credential)
            .then((result) => {
              const tokens = result._tokenResponse;
              const tokenStrings = JSON.stringify({
                access_token: tokens.oauthAccessToken,
                id_token: tokens.idToken,
                expires_in: 3599,
                token_type: 'Bearer',
                scope:
                  'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid https://www.googleapis.com/auth/calendar.readonly',
                refresh_token: tokens.refreshToken,
              });
              fetch('https://ce.finow.io/v1/google/', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: tokenStrings,
              })
                .then((response) => response.json())
                .then((data) => {
                  sendResponse({ message: 'Successful', data, auth_token });
                  chrome.storage.sync.set({ bearer_token: data.accessToken });
                })
                .catch((err) => {
                  sendResponse({
                    error_message: 'Failed',
                    data: null,
                    error: err,
                  });
                });
            })
            .catch((err) => console.error(err));
        }
      );
    } else if (request.message === 'fetchMeetings') {
      chrome.storage.sync.get(['bearer_token'], function (result) {
        fetch('https://ce.finow.io/v1/calendar/', {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${result.bearer_token}`,
          },
        })
          .then((response) => response.json())
          .then((data) => {
            sendResponse({
              message: 'Successful',
              data,
            });
          })
          .catch((err) => {
            console.log({
              message: 'Failed to get meetings',
              error: err,
            });
          });
      });
    }

    return true;
  });

  function injectscript() {
    const foreground_entry_point = document.createElement('div');
    let reactJS_script = document.createElement('script');
    foreground_entry_point.id = 'content';
    foreground_entry_point.appendChild(reactJS_script);
    document.body.appendChild(foreground_entry_point);
  }

  chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete' && tab.url.includes('http')) {
      chrome.scripting.executeScript(
        { target: { tabId: tabId }, func: injectscript },
        function () {
          chrome.scripting.executeScript(
            { target: { tabId: tabId }, files: ['ContentScript.bundle.js'] },
            function () {
              console.log('INJECTED AND EXECUTED');
            }
          );
        }
      );
    }
  });
} catch (err) {
  console.error(err);
}
